#include "Arduino.h"
#include "Robot_Constants.h"
#include "IRRemote.h"

IRRemote::IRRemote(int pin)
{
	pinMode(pin, INPUT);
	_pin = pin;
	_lastvalue = 0;
}

int IRRemote::read()
{
	int input = analogRead(_pin);
	_lastvalue = input;
	return input;
}