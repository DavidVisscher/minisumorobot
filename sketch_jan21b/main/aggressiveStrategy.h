#ifndef aggressiveStrategy_h
#define aggressiveStrategy_h

#include "Strategy.h"
#include "Wheel.h"

class AggressiveStrategy : public Strategy
{
public:
	AggressiveStrategy();
	virtual String act(Wheel* wheel1, Wheel* wheel2, int ir_left_data, int ir_right_data, int sonar1_ping, int sonar2_ping );
	String _debug_status;
};
#endif
