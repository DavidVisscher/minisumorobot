#ifndef IRRemote_H
#define IRRemote_H

class IRRemote
{
	public:
		IRRemote(int pin);
		int _pin;
		int _lastvalue;
		int read();
};

#endif