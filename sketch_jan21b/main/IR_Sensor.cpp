#include "Arduino.h"
#include "IR_Sensor.h"

IRSensor::IRSensor(int pin)
{
  pinMode(pin, INPUT);
  _pin = pin;
}

int IRSensor::detect()
{
  return (uint8_t)analogRead(_pin);
}

