#include "Robot_Constants.h"
#include "NewPing.h"
#include "Wheel.h"
#include "IR_Sensor.h"
#include "AggressiveStrategy.h"
#include "DefensiveStrategy.h"
#include "IRRemote.h"

NewPing sonar1(SENSOR1_TRIG_PIN,SENSOR1_ECHO_PIN,SENSOR_MAX_DISTANCE_CM);
NewPing sonar2(SENSOR2_TRIG_PIN,SENSOR2_ECHO_PIN,SENSOR_MAX_DISTANCE_CM);

Wheel* wheel1 = new Wheel(HB_IN1, HB_IN2, HB_PWM1);
Wheel* wheel2 = new Wheel(HB_IN3, HB_IN4, HB_PWM2);

IRSensor* ir_right = new IRSensor(IR1_PIN); //right
IRSensor* ir_left = new IRSensor(IR2_PIN); //left

Strategy* strategy;

IRRemote* ir_remote = new IRRemote(IR_REMOTE_PIN);

int current_strat;

void setup() {
	// put your setup code here, to run once:
	#if DEBUG_SERIAL
	Serial.begin(9600);
	Serial.println("Alfons greets you");
	#endif

	pinMode(STRAT_SWITCH_PIN, INPUT);
	int strat = digitalRead(STRAT_SWITCH_PIN);
	if(strat == 1)
	{
		strategy = new AggressiveStrategy();
		current_strat = 1;
	}
	else
	{
		strategy = new DefensiveStrategy();
		current_strat = 0;
	}

	int remotedata;
	do{
	    remotedata = ir_remote->read();
	    #if DEBUG_SERIAL
	    Serial.println("Waiting for on signal: " + (String)remotedata);
		#endif
	} while (!(remotedata > IR_REMOTE_ON_MIN && remotedata < IR_REMOTE_ON_MAX));
	
}

void loop() {

	int strat = digitalRead(STRAT_SWITCH_PIN);

	if(strat != current_strat)
	{
		if(strat == 1)
		{
			strategy = (Strategy*)new AggressiveStrategy();
			current_strat = 1;
		}
		else
		{
			strategy = (Strategy*)new DefensiveStrategy();
			current_strat = 0;
		}
		#if DEBUG_SERIAL
		Serial.println("Strategy Changed to " + (String)current_strat);
		#endif
	}

	//Detect
	delay(20);
	int sonar1_ping = sonar1.ping_cm();
	delay(20);
	int sonar2_ping = sonar2.ping_cm();

	wheel1->setPower(128);
	wheel2->setPower(128);
	
	int ir_left_data = ir_left->detect();
	int ir_right_data = ir_right->detect();
	
	String debug_status = strategy->act(wheel1, wheel2, ir_left_data, ir_right_data, sonar1_ping, sonar2_ping );
	int remotedata = ir_remote->read();

	#if DEBUG_SERIAL
	Serial.write(27);       // ESC command
  	Serial.print("[2J");    // clear screen command
  	Serial.write(27);
 	Serial.print("[H");     // cursor to home command

	Serial.println("Sonar: " + (String)sonar2_ping + "cm & " + (String)sonar1_ping + " cm ");
	Serial.println("Status: [" + debug_status + "]" + " Strategy: " + (String)current_strat );
	Serial.println("IR: [" + (String)ir_left_data + "," + (String)ir_right_data +"], remote: "+ (String)remotedata);
	#endif
	
	#if DEBUG_DELAY
	delay(500); //slow down logic so it's easier to analyse
	#endif
}

