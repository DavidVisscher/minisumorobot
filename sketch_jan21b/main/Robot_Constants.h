#ifndef robot_constants_h
#define robot_constants_h

#define SENSOR1_TRIG_PIN 10
#define SENSOR1_ECHO_PIN 11
#define SENSOR2_TRIG_PIN 12
#define SENSOR2_ECHO_PIN 2

#define SENSOR_MAX_DISTANCE_CM 25
#define SENSOR_SENSITIVITY 5

#define HB_IN1 6
#define HB_IN2 7
#define HB_IN3 8
#define HB_IN4 9

#define HB_PWM1 5
#define HB_PWM2 3

#define STRAT_SWITCH_PIN 13

#define IR_THRESHOLD 127
#define IR1_PIN A0
#define IR2_PIN A1

#define WHEEL_SAFETY_THRESHOLD 45

#define DEBUG_SERIAL 1
#define DEBUG_DELAY 1

#define DEFENSIVE_STEERING_ANGLE 1.1

#define IR_REMOTE_PIN A2

#define IR_REMOTE_ON_SIGNAL 7
#define IR_REMOTE_ON_MIN 5
#define IR_REMOTE_ON_MAX 9

#endif