#include "Arduino.h"
#include "Wheel.h"
#include "Robot_Constants.h"

Wheel::Wheel(int pin1, int pin2, int pinpwm)
{
  pinMode(pin1, OUTPUT);
  pinMode(pin2, OUTPUT);
  pinMode(pinpwm, OUTPUT);

  _pin1 = pin1;
  _pin2 = pin2;
  _pinpwm = pinpwm;
};

void Wheel::forward()
{
  digitalWrite(_pin2,LOW);
  digitalWrite(_pin1,HIGH);
};

void Wheel::backward()
{
  digitalWrite(_pin1,LOW);
  digitalWrite(_pin2,HIGH);
};

void Wheel::brake()
{
  digitalWrite(_pin1,LOW);
  digitalWrite(_pin2,LOW);
};

void Wheel::setPower(int p)
{
  //ensure p is within 0-255
  if(p>255){
    p=255;
  }
  if(p<WHEEL_SAFETY_THRESHOLD){
    p=0;
  }

  analogWrite(_pinpwm,p);
};

