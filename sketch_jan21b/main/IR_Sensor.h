#ifndef IR_SENSOR_H
#define IR_SENSOR_H

class IRSensor
{
  public:
  IRSensor(int pin);
  int detect();
  private:
  int _pin;
};

#endif
