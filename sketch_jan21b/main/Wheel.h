#ifndef Wheel_h
#define Wheel_h

class Wheel
{
  public:
    Wheel(int pin1, int pin2, int pinpwm);
    void forward();
    void backward();
    void brake();
    void setPower(int power);
  private:
    int _pin1;
    int _pin2;
    int _pinpwm;
};

#endif
