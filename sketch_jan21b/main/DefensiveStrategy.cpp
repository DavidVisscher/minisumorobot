#include "Arduino.h"
#include "DefensiveStrategy.h"
#include "Robot_Constants.h"

DefensiveStrategy::DefensiveStrategy()
{
	_debug_status = "";
}

String DefensiveStrategy::act(Wheel* wheel1, Wheel* wheel2, int ir_left_data, int ir_right_data, int sonar1_ping, int sonar2_ping )
{
	int sonar_diff = sonar1_ping - sonar2_ping;
	
	bool ir_left_triggered = (ir_left_data > IR_THRESHOLD);
	bool ir_right_triggered = (ir_right_data > IR_THRESHOLD);

	if(ir_left_triggered && ir_right_triggered)
	{
		wheel1->backward();
		wheel1->setPower(100);
		wheel2->backward();
		wheel2->setPower(100*DEFENSIVE_STEERING_ANGLE);
		return "[Defensive Strategy] Next to line, going backward at an angle";
	}
	else if(ir_right_triggered)
	{
		//Search by rotating left
		wheel1->forward();
		wheel1->setPower(100);
		wheel2->forward();
		wheel2->setPower(100*DEFENSIVE_STEERING_ANGLE);
		return "[Defensive Strategy] Following Line";
	}
	else if(ir_left_triggered)
	{
		wheel1->backward();
		wheel1->setPower(100);
		wheel2->forward();
		wheel2->setPower(100);
		return "[Defensive Strategy] Wrong way round, rotating";
	}
	else
	{
		wheel1->forward();
		wheel2->forward();
		wheel1->setPower(100);
		wheel2->setPower(100);
		return "[Defensive Strategy] No line found, straight ahead";
	}
}