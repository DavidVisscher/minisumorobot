#ifndef strategy_h
#define strategy_h
#include "Wheel.h"

class Strategy
{
  public:
    Strategy();
    virtual String act(Wheel* wheel1, Wheel* wheel2, int ir_left_data, int ir_right_data, int sonar1_ping, int sonar2_ping ) = 0;
    String _debug_status;
};

#endif
