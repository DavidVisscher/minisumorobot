#include "Arduino.h"
#include "AggressiveStrategy.h"
#include "Robot_Constants.h"

AggressiveStrategy::AggressiveStrategy() : Strategy()
{
	_debug_status = "Aggressive";
}

String AggressiveStrategy::act(Wheel* wheel1, Wheel* wheel2, int ir_left_data, int ir_right_data, int sonar1_ping, int sonar2_ping )
{
	int sonar_diff = sonar1_ping - sonar2_ping;
	//react
	if(ir_left_data > IR_THRESHOLD)
	{
		wheel1->forward();
		wheel2->backward();
		return "[Aggressive Strategy]left_ir_detected";
	}
	else if (ir_right_data > IR_THRESHOLD)
	{
		wheel1->backward();
		wheel2->forward();
		return "[Aggressive Strategy]right_ir_detected";
	}
	else if(ir_left_data > IR_THRESHOLD && ir_right_data > IR_THRESHOLD)
	{
		wheel1->backward();
		wheel2->backward();
		return "[Aggressive Strategy]both_ir_detected";
	}
	else if(sonar1_ping == 0 && sonar2_ping == 0) //nothing detected
	{
	//Search by rotating left
		wheel1->backward();
		wheel2->forward();
		return "[Aggressive Strategy]searching";
	}
	else if(sonar_diff < SENSOR_SENSITIVITY && sonar_diff > -SENSOR_SENSITIVITY) // if opponent in front
	{
	//move forward
		wheel1->forward();
		wheel2->forward();
		return "[Aggressive Strategy]ramming";
	}
	else if(sonar_diff >=SENSOR_SENSITIVITY) //if opponent to the right
	{
		wheel1->forward();
		wheel2->backward();
		return "[Aggressive Strategy]rotate right";
	}
	else if(sonar_diff <= -SENSOR_SENSITIVITY) //if opponent to the left
	{
		wheel1->backward();
		wheel2->forward();
		return "[Aggressive Strategy]rotate left";
	}
}